import React from 'react'

export const Title = () => {
    
    const style={
        margin: 20,
        textAlign: 'center'
    }
    
    return (
        
        <div style={style}>

        
            <h1>
            CRUD example
            </h1>
        </div>
    )
}

export default Title